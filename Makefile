##################################################################################
# WHATEVER YOU DO, PLEASE DO IT TO Makefile.config AND LEAVE THIS MAKEFILE ALONE #
##################################################################################
include Makefile.config


#------------------------------------------------------------------#
# the menu                                                         #
#------------------------------------------------------------------#
MASTER_DEFINEFLAGS	=	$(DEFINEFLAGS)

#------------------------------------------------------------------#
# make settings available to all other Makefiles                   #
#------------------------------------------------------------------#
export CC
export FC
export OPTIMIZE
export CCFLAGS
export LNFLAGS
export MASTER_DEFINEFLAGS
export MAKE

# everything in src/
#===================
Vweb:	FORCE dirs
	cd src;\
	${MAKE} Vweb;\
	mv -f Vweb ../bin/$(EXEC)

# everything in analysis/
#=======================
Vweb2ascii:	FORCE dirs
	cd analysis;\
	${MAKE} Vweb2ascii;\
	mv -f Vweb2ascii ../bin

Vwebhalos:	FORCE dirs
	cd analysis;\
	${MAKE} Vwebhalos;\
	mv -f Vwebhalos ../bin

Vwebfilaments:	FORCE dirs
	cd analysis;\
	${MAKE} Vwebfilaments;\
	mv -f Vwebfilaments ../bin

#-------------------------------------------------------------------#
# "make clean"
#-------------------------------------------------------------------#
clean:	FORCE
	@echo '';\
	echo '*=======================================================================*';\
	echo ' ${MAKE} clean: removing all unneeded files';\
	echo '*=======================================================================*';\
	echo '';\
	echo ''
	cd src; ${MAKE} clean

#-------------------------------------------------------------------#
# "make veryclean"
#-------------------------------------------------------------------#
veryclean:	FORCE
	@echo '';\
	echo '*=======================================================================*';\
	echo ' ${MAKE} veryclean: removing all unneeded files (incl. emacs backup files!)';\
	echo '*=======================================================================*';\
	echo '';\
	echo ''
	rm -f *~ *~.*;\
	cd src; ${MAKE} veryclean

FORCE:

dirs:
	mkdir -p bin
