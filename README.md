# README

This program classifying the large-scale structure into knots, filaments, sheets and voids
by means of calculating hessian matrix of velocity shear (Vweb) and of potential (Pweb).

### Notices

Don't you use parallel calculation right now, there maybe some problems. The "Standard SQL"
in the Makefile.config also has problems to compile the code... which needs to be fixed.
Only use Debug right now!!!

### Version

-   0.1 -- works with both potential and velocity shear. Only TOP-hat smoothing is adopted...
-   0.11 -- currently working on adding mesh reading process.
-   0.2 -- replaced the numperical recipes fft with fftw3. Therefore, it needs fftw3 installed with header.

### How do I get set up?

-   Summary of set up
-   Configuration
-   Dependencies

    require fftw3 with header installed.
    
-   Database configuration
-   How to run tests
-   Deployment instructions

### Contribution guidelines

-   Writing tests
-   Code review
-   Other guidelines

### Who do I talk to?

-   Repo owner or admin -- cuiweiguang@gmail.com
