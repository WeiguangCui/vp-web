import struct
import numpy as np


def readVweb(fileall, endian=None, UonGrid=None, DWEB=None, quiet=None, selected=None):
    """
    readVweb(fileall,endian=None,UonGrid=None):
    read Vweb binary out puts
    fileall: path + filename
             last 4 charaters "Vweb" means read only one file
             else try to read multi-files by adding ".000x.Vweb" to fileall
    endian:  default '='
    UonGrid: default 34
    quiet  : default False
    selected:select Return data colomn.
        e.g. selected=[0,1,2] will Return x,y,z
    Return : data
    Structure of returned data:
    typedef struct {
        float x;0
        float y;1    // cell-centre [kpc/h]
        float z;2
        float dens;3 // overdensity respected to mean density
        float Vx;4
        float Vy;5   // velocity [km/sec]
        float Vz;6
        float Wx;7
        float Wy;8   // vorticity [km/sec]
        float Wz;9
        float lambda1;10
        float lambda2;11
        float lambda3;12
        float local_shear[3][3];13:22
    #ifdef DWEB
        float lambda1;22
        float lambda2;23
        float lambda3;24
        float local_shear[3][3];25:34
    #endif
    #ifdef UonGrid
            float u;34
    #endif
    } Vweb_t;
    """

    if endian is None:
        endian = '='
    dims = 22
    if DWEB:
        dims += 12
    if UonGrid:
        dims += 1

    if fileall[-4:] == "Vweb":  # read only one file
        opf = open(fileall, 'rb')

        swap = struct.unpack(endian + 'i', opf.read(4))[0]
        Nnodes = struct.unpack(endian + 'q', opf.read(8))[0]
        L = struct.unpack(endian + 'q', opf.read(8))[0]
        BoxSize = struct.unpack(endian + 'f', opf.read(4))[0]
        if quiet:
            quiet = True
        else:
            print("Nnodes is: ", Nnodes)
            print("L is: ", L)
            print("Boxsize is: ", BoxSize)

        data = np.ndarray(shape=(Nnodes, dims), dtype="float32",
                          buffer=opf.read(Nnodes * dims * 4))
        opf.close()

    else:  # try to read multi-files
        exts = "0000"
        rnod, rfil = 0, 0
        ttnd = 10
        while (rnod < ttnd - 1):
            tmpe = exts + str(rfil)
            newf = fileall + "." + tmpe[-4:] + ".Vweb"
            try:
                opf = open(newf, 'rb')
            except IOError:
                print('cannot open', newf)
                raise AttributeError('Please check and try again')

            swap = struct.unpack(endian + 'i', opf.read(4))[0]
            Nnodes = struct.unpack(endian + 'q', opf.read(8))[0]
            L = struct.unpack(endian + 'q', opf.read(8))[0]
            BoxSize = struct.unpack(endian + 'f', opf.read(4))[0]
            if quiet:
                quiet = True
            else:
                print("Nnodes is: ", Nnodes)
                print("L is: ", L)
                print("Boxsize is: ", BoxSize)

            if rnod == 0:
                ttnd = L**3
                data = np.zeros((ttnd, dims), dtype='float32')

            data[rnod:rnod + Nnodes] = \
                np.ndarray(shape=(Nnodes, dims), dtype="float32",
                           buffer=opf.read(Nnodes * dims * 4))
            rnod += Nnodes
            rfil += 1
            opf.close()

    del(swap)
    if selected is None:
        return(data)
    else:
        return(data[:, selected])
