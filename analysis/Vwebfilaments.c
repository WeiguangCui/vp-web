#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>

#include "../src/param.h"
#include "../src/tdef.h"
#include "../src/common.c"
#include "../src/libutility/utility.h"

//=================================================================================
// DEFINEFLAGS / CODE FEATURES
//=================================================================================
#define VWEB_ASCII  // we still allow the Vweb to be in ASCII format
//#define RECURSION // not functional: the recursion goes to deep so that we get a stack overflow
//#define DEBUG

//=================================================================================
// parameters
//=================================================================================
#define NMIN 125    // minimum number of nodes per filament


//===================================================================
// the headers for the ASCII output files
//===================================================================
#define HEADER_FILAMENTS    "#id(1) Nnodes(2) x(3) y(4) z(5) b(6) Ebx(7) Eby(8) Ebz(9) c(10) Ecx(11) Ecy(12) Ecz(13)"
#define HEADER_CELLS        "#FilamentID(1) Nnodes(2)\n#  x(1) y(2) z(3)"

//===================================================================
// structures
//===================================================================
typedef struct VWEB_STRUCTURE *Vweb_tPtr;
typedef struct VWEB_STRUCTURE {
  float x;
  float y;    // cell-centre [kpc/h]
  float z;
  float dens; // overdensity
  float Vx;
  float Vy;   // velocity [km/sec]
  float Vz;
  float Wx;
  float Wy;   // vorticity [km/sec]
  float Wz;
  float lambda1;
  float lambda2;
  float lambda3;
  float local_shear[3][3];
#ifdef UonGrid
  float u;
#endif
  
  int8_t  type;     // this is being determined while reading the Vweb file
  int8_t  type_new; // but we remove all but filament nodes for the percolation
  
  int64_t   filaid;   // to which filament does the node belong
} Vweb_t;

typedef struct FILAMENT_STRUCTURE {
  uint64_t  filaid;
  uint64_t  Nnodes;  // how many Vweb nodes belong to filament
  uint64_t *idx;     // which Vweb nodes belong to filament
  
  float     x;
  float     y;
  float     z;
  float     a;
  float     Ea[3];
  float     b;
  float     Eb[3];
  float     c;
  float     Ec[3];
} Filament_t;

//===================================================================
// global variables
//===================================================================
uint64_t Nknots;
uint64_t Nfilaments;
uint64_t Nsheets;
uint64_t Nvoids;

double   lambda_threshold;
uint64_t NN;
uint64_t l1dim;

float    BoxSize;

Vweb_t  *Vweb;
uint64_t VwebNodes;

Filament_t *Filament;
uint64_t    nFilaments;

//===================================================================
// prototypes and the likes
//===================================================================
#define VwebIndex(i,j,k)  ((i)+(j)*l1dim+(k)*l1dim*l1dim)

#define KNOT_TYPE     3
#define FILAMENT_TYPE 2
#define SHEET_TYPE    1
#define VOID_TYPE     0

void     get_Vweb           (int32_t , char **);
void     read_Vweb          (FILE *);
void     get_ijk            (float, float, float, uint64_t *, uint64_t *, uint64_t *);
void     friends_of_friends (uint64_t, uint64_t, uint64_t);

//===================================================================
// routines copied from AHF to keep this code independent
//===================================================================
int     ReadUInt32         (FILE *, uint32_t *,      int);
int     ReadUInt64         (FILE *, uint64_t      *, int);
//int     ReadFloat          (FILE *, float         *, int);
#define TRUE  1
#define FALSE 0
#define pow3(x) ((x)*(x)*(x))

/*=================================================================================================
 * main()
 *=================================================================================================*/
int main(argc,argv)
int argc;
char **argv;
{   
  char        outfile[2048], **infile;
  char        Vwebprefix[2048];
  FILE       *fpout;
  int32_t     slen, Vwebnfiles;
  int64_t     i,j,k,in,jn,kn,ii,jj,kk,iVweb,idx,jdx,iFila,iFilament,Noffset;
  time_t      elapsed = (time_t)0;
  Vweb_t     *curVwebPtr;

  double      itensor[3][3];
	double      a11, a22, a33, a12, a13, a23, a, b, c;
  double      x, y, z, dx, dy, dz;

  fprintf(stderr,"======================================================================\n");
  fprintf(stderr,"                      Vweb based filament finder\n");
  fprintf(stderr,"              (currently only works for regular Vweb grids!)\n");
  fprintf(stderr,"======================================================================\n");

  
  
  
  //===================================================================
  // deal with command line
  //===================================================================
#ifdef VWEB_ASCII
  if(argc<5)
   {
    fprintf(stderr,"usage: %s Vwebfile lambda_threshold NN BoxSize\n",*argv);
    exit(1);
   }
  strcpy(Vwebprefix,      argv[1]);
  lambda_threshold = atof(argv[2]);
  NN               = (uint64_t) atoi(argv[3]);
  BoxSize          = atof(argv[4]);
  Vwebnfiles       = 1;
#else
  if(argc<4)
    {
      fprintf(stderr,"usage: %s Vwebprefix Vwebnfiles lambda_threshold\n",*argv);
      exit(1);
    }
  strcpy(Vwebprefix,      argv[1]);
  Vwebnfiles       = atoi(argv[2]);
  lambda_threshold = atof(argv[3]);
  BoxSize          = -1;
#endif  
  
  
  
  
  
  
  
  
  
  
  
  //===================================================================
  // read all Vweb files into memory
  //===================================================================
  elapsed  = (time_t)0;
  elapsed -= time(NULL);

  // prepare array holding filenames
  infile = (char **) calloc(Vwebnfiles, sizeof(char *));
  for (i=0; i<Vwebnfiles; i++) {
    infile[i] = (char *) calloc(2048, sizeof(char));
  }

  // construct filenames
  if(Vwebnfiles == 1) {
#ifdef VWEB_ASCII
    sprintf(infile[0],"%s",Vwebprefix);
#else
    sprintf(infile[0],"%s.Vweb",Vwebprefix);
#endif
  }
  else {
    for(i=0; i<Vwebnfiles; i++) {
      sprintf(infile[i],"%s.%04d.Vweb",Vwebprefix,(int)i);
    }
  }
  
  // be verbose
  fprintf(stderr,"o reading Vweb from:\n");
  for(i=0; i<Vwebnfiles; i++) {
    fprintf(stderr,"   %s\n",infile[i]);
  }
  
  // pass filenames to general purpose reading routine
  get_Vweb(Vwebnfiles, infile);

  // free infile[]
  for (i=0; i<Vwebnfiles; i++) {
    free(infile[i]);
  }
  free(infile);
  
  elapsed += time(NULL);
  fprintf(stderr,"   done in %ld sec.\n",elapsed);
  
  
  
  
  
  
  

  
  
  
  
  
  
  //===================================================================
  // tag all knots (and their neighbours), sheets, and voids as type = -1
  //===================================================================
  elapsed  = (time_t)0;
  elapsed -= time(NULL);
  
  fprintf(stderr,"o tagging voids, sheets, and knots as type -1 ... ");
  for(iVweb=0; iVweb<VwebNodes; iVweb++) {
    
    // knots
    if(Vweb[iVweb].type == KNOT_TYPE) {
      get_ijk(Vweb[iVweb].x,Vweb[iVweb].y,Vweb[iVweb].z,&i,&j,&k);      
      for(in=i-NN; in<=i+NN; in++) {
        for(jn=j-NN; jn<=j+NN; jn++) {
          for(kn=k-NN; kn<=k+NN; kn++) {
            ii  = (in+l1dim) % (l1dim);
            jj  = (jn+l1dim) % (l1dim);
            kk  = (kn+l1dim) % (l1dim);
            idx = VwebIndex(ii,jj,kk);
            Vweb[idx].type_new = -1;
          }
        }
      }
    }
    // voids and sheets
    else if(Vweb[iVweb].type == VOID_TYPE || Vweb[iVweb].type == SHEET_TYPE) {
      Vweb[iVweb].type_new = -1;
    }
    // filaments (they keep their type)
    else if (Vweb[iVweb].type == FILAMENT_TYPE && Vweb[iVweb].type_new != -1) {
      Vweb[iVweb].type_new = Vweb[iVweb].type;
    }
  }
  elapsed += time(NULL);
  fprintf(stderr,"done in %ld sec.\n",elapsed);
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  //===================================================================
  // percolate filament nodes generating filament structures
  //===================================================================
  elapsed  = (time_t)0;
  elapsed -= time(NULL);
  
  fprintf(stderr,"o percolating filament nodes ... ");
  Filament   = NULL;
  nFilaments = 0;
  for(iVweb=0; iVweb<VwebNodes; iVweb++) {
    
    // is node of type filament
    if(Vweb[iVweb].type_new == FILAMENT_TYPE) {

      // unmarked node
      if(Vweb[iVweb].filaid == -1) {
      
        // start a new filament
        nFilaments++;
        Filament = (Filament_t *) realloc(Filament, nFilaments*sizeof(Filament_t));
        
        // add current node to filament
        Filament[nFilaments-1].Nnodes = 1;
        Filament[nFilaments-1].filaid = nFilaments;
        Filament[nFilaments-1].idx    = (uint64_t *) calloc(1, sizeof(uint64_t));
        Filament[nFilaments-1].idx[0] = iVweb;
        
        // mark Vweb nodes as belonging to this filament (using nFilaments as the ID of the filament)
        Vweb[iVweb].filaid = nFilaments;
        
        // mark all (filament) neighbours
        get_ijk(Vweb[iVweb].x,Vweb[iVweb].y,Vweb[iVweb].z,&i,&j,&k);
        for(in=i-1; in<=i+1; in++) {
          for(jn=j-1; jn<=j+1; jn++) {
            for(kn=k-1; kn<=k+1; kn++) {
              ii  = (in+l1dim) % (l1dim);
              jj  = (jn+l1dim) % (l1dim);
              kk  = (kn+l1dim) % (l1dim);
              idx = VwebIndex(ii,jj,kk);
              
              if(Vweb[idx].type_new == FILAMENT_TYPE) {
                
                // 1. node has not been marked yet
                if(Vweb[idx].filaid == -1) {                  
                  // add current node to filament
                  Vweb[idx].filaid = nFilaments;
                  Filament[nFilaments-1].Nnodes++;
                  Filament[nFilaments-1].idx    = (uint64_t *) realloc(Filament[nFilaments-1].idx, Filament[nFilaments-1].Nnodes*sizeof(uint64_t));
                  Filament[nFilaments-1].idx[Filament[nFilaments-1].Nnodes-1] = idx;
                }
                
                // 2. node already belongs to filament
                else if (Vweb[idx].filaid == nFilaments) {
                  //fprintf(stderr,"    -> do nothing\n");
                }
                
                // 3. two disconnected filaments touch => merge!
                else {
                  iFilament = Vweb[idx].filaid;
                  Noffset = Filament[nFilaments-1].Nnodes;
                  Filament[nFilaments-1].Nnodes += Filament[iFilament-1].Nnodes;
                  Filament[nFilaments-1].idx     = (uint64_t *) realloc(Filament[nFilaments-1].idx, Filament[nFilaments-1].Nnodes*sizeof(uint64_t));

#pragma omp parallel for private(iFila,jdx) shared(Filament,nFilaments,iFilament,Vweb,Noffset) schedule(static)
                  for(iFila=0; iFila<Filament[iFilament-1].Nnodes; iFila++) {
                    jdx = Filament[iFilament-1].idx[iFila];
                    Filament[nFilaments-1].idx[Noffset+iFila] = jdx;
                    Vweb[jdx].filaid = nFilaments;
                  }
                  
                  // remove memory attached to this filament
                  Filament[iFilament-1].Nnodes = 0;
                  free(Filament[iFilament-1].idx);
                  Filament[iFilament-1].idx = NULL;
                }
                
              } //if(FILAMENT_TYPE)
            } //kn
          } //jn
        } //in

        
        
#ifdef RECURSION
        // search for friends-of-friends of current node
        // (automatically adding them to Filament[])
        get_ijk(Vweb[iVweb].x,Vweb[iVweb].y,Vweb[iVweb].z, &i,&j,&k);
        friends_of_friends(i,j,k);
#endif
      }
      
    }

  }
  elapsed += time(NULL);
  fprintf(stderr,"done in %ld sec.\n",elapsed);

  
  
  
  
  
  
  
  
  
  
  
  
  
  //===================================================================
  // calculate filament properties
  //===================================================================
  elapsed  = (time_t)0;
  elapsed -= time(NULL);
  fprintf(stderr,"o calculating filament properties ... ");
  
//#pragma omp parallel for schedule(dynamic) private(iFila,x,y,z,i,idx,a11,a22,a33,a12,a13,a23,dx,dy,dz) shared(Filaments,nFilaments)
  for(iFila=0; iFila<nFilaments; iFila++) {
        
    if(Filament[iFila].Nnodes > NMIN) {
    
      // goemtrical "centre-of-mass"
      //=============================
      x = 0.; y = 0.; z = 0.;
      for(i=0; i<Filament[iFila].Nnodes; i++) {
        idx = Filament[iFila].idx[i];

        x += Vweb[idx].x;
        y += Vweb[idx].y;
        z += Vweb[idx].z;
      }
      x /= (double)Filament[iFila].Nnodes; y /= (double)Filament[iFila].Nnodes; z /= (double)Filament[iFila].Nnodes;
      
    
      // moment of inertia tensor
      //==========================
      a11 = 0.;
      a22 = 0.;
      a33 = 0.;
      a12 = 0.;
      a13 = 0.;
      a23 = 0.;
      
      for(i=0; i<Filament[iFila].Nnodes; i++) {
        idx = Filament[iFila].idx[i];
        
        dx = Vweb[idx].x - x;
        dy = Vweb[idx].y - y;
        dz = Vweb[idx].z - z;
        
        a11 += dx * dx;
        a22 += dy * dy;
        a33 += dz * dz;
        a12 += dx * dy;
        a13 += dx * dz;
        a23 += dy * dz;
        
        itensor[0][0] = a11;
        itensor[1][1] = a22;
        itensor[2][2] = a33;
        itensor[0][1] = a12;
        itensor[1][0] = a12;
        itensor[0][2] = a13;
        itensor[2][0] = a13;
        itensor[1][2] = a23;
        itensor[2][1] = a23;
        get_axes(itensor, &a, &b, &c);
        
      }
      
      // copy temporal properties over to Filament[]
      Filament[iFila].x     = x;
      Filament[iFila].y     = y;
      Filament[iFila].z     = z;
      Filament[iFila].a     = a;
      Filament[iFila].Ea[0] = itensor[0][0];
      Filament[iFila].Ea[1] = itensor[1][0];
      Filament[iFila].Ea[2] = itensor[2][0];
      Filament[iFila].b     = b;
      Filament[iFila].Eb[0] = itensor[0][1];
      Filament[iFila].Eb[1] = itensor[1][1];
      Filament[iFila].Eb[2] = itensor[2][1];
      Filament[iFila].c     = c;
      Filament[iFila].Ec[0] = itensor[0][2];
      Filament[iFila].Ec[1] = itensor[1][2];
      Filament[iFila].Ec[2] = itensor[2][2];
    } // if(Nnodes > NMIN)
    else {
      // too few particles to calculate any property
      Filament[iFila].x     = 0.;
      Filament[iFila].y     = 0.;
      Filament[iFila].z     = 0.;
      Filament[iFila].a     = 0.;
      Filament[iFila].Ea[0] = 0.;
      Filament[iFila].Ea[1] = 0.;
      Filament[iFila].Ea[2] = 0.;
      Filament[iFila].b     = 0.;
      Filament[iFila].Eb[0] = 0.;
      Filament[iFila].Eb[1] = 0.;
      Filament[iFila].Eb[2] = 0.;
      Filament[iFila].c     = 0.;
      Filament[iFila].Ec[0] = 0.;
      Filament[iFila].Ec[1] = 0.;
      Filament[iFila].Ec[2] = 0.;
    }
  }
 
  elapsed += time(NULL);
  fprintf(stderr,"   done in %ld sec.\n",elapsed);

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  //===================================================================
  // writing output files
  //===================================================================
  
  // 1. _cells file
  //================
  elapsed -= time(NULL);
  sprintf(outfile,"%s-Vweb%4.2f-NN%02d_cells",Vwebprefix,lambda_threshold,NN);
  fprintf(stderr,"o writing filament nodes to: %s ... ",outfile);
  
  // open output file
  fpout = fopen(outfile,"w");
  if(fpout == NULL) {
    fprintf(stderr,"FATAL: cannot open %s for writing\n",outfile);
    exit(0);
  }
  
  // header
  fprintf(fpout,"%s\n",HEADER_CELLS);
  
  // loop over all isolated filaments
  for(iFila=0; iFila<nFilaments; iFila++) {
    // only write filaments that have not been merged with another one
    if(Filament[iFila].Nnodes > NMIN) {
      
      fprintf(fpout,"%"PRIu64" %"PRIu64"\n",iFila,Filament[iFila].Nnodes);
      
      // loop over all nodes belonging to this filament
      for(iVweb=0; iVweb<Filament[iFila].Nnodes; iVweb++) {
        idx = Filament[iFila].idx[iVweb];
        fprintf(fpout,"%f %f %f\n",Vweb[idx].x,Vweb[idx].y,Vweb[idx].z);
      } // iVweb
    } //if()
  } // iFila
  fclose(fpout);
  elapsed += time(NULL);
  fprintf(stderr,"done in %ld sec.\n",elapsed);
  
  
  // 2. _filaments file
  //====================
  elapsed -= time(NULL);
  sprintf(outfile,"%s-Vweb%4.2f-NN%02d_filaments",Vwebprefix,lambda_threshold,NN);
  fprintf(stderr,"o writing filament properties to: %s ... ",outfile);
  
  // open output file
  fpout = fopen(outfile,"w");
  if(fpout == NULL) {
    fprintf(stderr,"FATAL: cannot open %s for writing\n",outfile);
    exit(0);
  }
  
  // header
  fprintf(fpout,"%s\n",HEADER_FILAMENTS);

  // loop over all isolated filaments
  for(iFila=0; iFila<nFilaments; iFila++) {
    // only write filaments that have not been merged with another one
    if(Filament[iFila].Nnodes > NMIN) {
      
      fprintf(fpout,"%"PRIu64" %"PRIu64" %f %f %f %f %f %f %f %f %f %f %f\n",
              Filament[iFila].filaid,
              Filament[iFila].Nnodes,
              Filament[iFila].x,
              Filament[iFila].y,
              Filament[iFila].z,
              sqrt(Filament[iFila].b/Filament[iFila].a),
              Filament[iFila].Eb[0],
              Filament[iFila].Eb[1],
              Filament[iFila].Eb[2],
              sqrt(Filament[iFila].c/Filament[iFila].a),
              Filament[iFila].Ec[0],
              Filament[iFila].Ec[1],
              Filament[iFila].Ec[2]
              );
    } //if(Nnodes>0)
  } // iFila
  fclose(fpout);
  elapsed += time(NULL);
  fprintf(stderr,"done in %ld sec.\n",elapsed);
  
  
  
  
  //===================================================================
  // cleaning up
  //===================================================================
  if (Vweb)     free(Vweb);
  for(iFila=0; iFila<nFilaments; iFila++) {
    if(Filament[iFila].idx) free(Filament[iFila].idx);
  }
  if (Filament) free(Filament);

  fprintf(stderr,"END\n");
}
  
/*=================================================================================================
 * get_Vweb()
 *=================================================================================================*/
void  get_Vweb(int32_t nfiles, char **infile)
{
  int32_t i;
  FILE   *fpin;
  
  // for volume fraction statistics
  Nknots           = 0;
  Nfilaments       = 0;
  Nsheets          = 0;
  Nvoids           = 0;
  l1dim            = 0;
  
  // loop over all input files
  Vweb    = NULL;
  for(i=0; i<nfiles; i++) {
    
    // open input file
    fpin = fopen(infile[i],"rb");
    if(fpin == NULL) {
      fprintf(stderr,"FATAL: cannot open %s for reading\n",infile[i]);
      exit(0);
    }
    
    // read cosmic web updating Vweb and VwebNodes
    read_Vweb(fpin);
    fclose(fpin);
  }
  
  // double check for inconsistencies
  if(l1dim*l1dim*l1dim != VwebNodes) {
    fprintf(stderr,"you read a different number of nodes than expected: l1dim*l1dim*l1dim=%"PRIu64" vs. VwebNodes=%"PRIu64"\nABORTING\n",l1dim*l1dim*l1dim,VwebNodes);
    exit(0);
  }
  
  // be verbose
  fprintf(stderr,"\nVolume Filling Fractions: (grid=%"PRIu64", LambdaThreshold=%g)\n",l1dim,lambda_threshold);
  fprintf(stderr,"=========================\n");
  fprintf(stderr,"knots     = %lf\n",(double)Nknots/(double)pow3(l1dim));
  fprintf(stderr,"filaments = %lf\n",(double)Nfilaments/(double)pow3(l1dim));
  fprintf(stderr,"sheets    = %lf\n",(double)Nsheets/(double)pow3(l1dim));
  fprintf(stderr,"voids     = %lf\n",(double)Nvoids/(double)pow3(l1dim));
  fprintf(stderr,"knots     = %"PRIu64"\n",Nknots);
  fprintf(stderr,"filaments = %"PRIu64"\n",Nfilaments);
  fprintf(stderr,"sheets    = %"PRIu64"\n",Nsheets);
  fprintf(stderr,"voids     = %"PRIu64"\n",Nvoids);
  fprintf(stderr,"BoxSize   = %f\n\n",BoxSize);
}

/*=================================================================================================
 * read_Vweb()
 *=================================================================================================*/
void read_Vweb(FILE *fpin)
{
  uint32_t numColumns;
  uint64_t i,j,k,n,idx;
  int32_t  one;
  int      swap=0;
  uint64_t Nnodes;
  Vweb_t   Vweb_tmp;
  float    BoxSize_tmp;
  char     buffer[2048];
#ifdef DEBUG
  uint64_t ii,jj,kk;
#endif
  
  
#ifdef VWEB_ASCII
  // figure out number of cells
  Nnodes = 0;
  while(fgets(buffer,2048,fpin) != NULL) {
    Nnodes++;
  }
  rewind(fpin);

  // allocate space for Vweb
  // TODO: here we assume that there is only *one* ASCII file
  if(Vweb == NULL) {
    Vweb = (Vweb_t *) calloc(Nnodes, sizeof(Vweb_t));
  }
  VwebNodes = Nnodes;
  l1dim     = (uint64_t) pow((double)Nnodes,(1./3.)) + 1;
  
  // eventually read Vweb
  for(n=0; n<Nnodes; n++) {
    fgets(buffer,2048,fpin);
    sscanf(buffer,"%f %f %f %"SCNi64" %"SCNi64" %"SCNi64,
           &(Vweb_tmp.lambda1),
           &(Vweb_tmp.lambda2),
           &(Vweb_tmp.lambda3),
           &(i),
           &(j),
           &(k)        );
    
    // back to C-notation, please
    i--;
    j--;
    k--;
    
    // copy over to actual Vweb[] array
    idx = VwebIndex(i,j,k);
    Vweb[idx].x                 = ((double)i+0.5)*BoxSize/(double)l1dim;
    Vweb[idx].y                 = ((double)j+0.5)*BoxSize/(double)l1dim;
    Vweb[idx].z                 = ((double)k+0.5)*BoxSize/(double)l1dim;
#ifdef DEBUG
    get_ijk(Vweb[idx].x,Vweb[idx].y,Vweb[idx].z,&ii,&jj,&kk);
    if(i != ii || j != jj || k != kk) {
      fprintf(stderr,"%f %f %f %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" \n",
              Vweb[idx].x,Vweb[idx].y,Vweb[idx].z,
              i,j,k,ii,jj,kk);
      exit(0);
    }
#endif
    Vweb[idx].dens              = Vweb_tmp.dens;
    Vweb[idx].Vx                = Vweb_tmp.Vx;
    Vweb[idx].Vy                = Vweb_tmp.Vy;
    Vweb[idx].Vz                = Vweb_tmp.Vz;
    Vweb[idx].Wx                = Vweb_tmp.Wx;
    Vweb[idx].Wy                = Vweb_tmp.Wy;
    Vweb[idx].Wz                = Vweb_tmp.Wz;
    Vweb[idx].lambda1           = Vweb_tmp.lambda1;
    Vweb[idx].lambda2           = Vweb_tmp.lambda2;
    Vweb[idx].lambda3           = Vweb_tmp.lambda3;
    Vweb[idx].local_shear[0][0] = Vweb_tmp.local_shear[0][0];
    Vweb[idx].local_shear[1][0] = Vweb_tmp.local_shear[1][0];
    Vweb[idx].local_shear[2][0] = Vweb_tmp.local_shear[2][0];
    Vweb[idx].local_shear[0][1] = Vweb_tmp.local_shear[0][1];
    Vweb[idx].local_shear[1][1] = Vweb_tmp.local_shear[1][1];
    Vweb[idx].local_shear[2][1] = Vweb_tmp.local_shear[2][1];
    Vweb[idx].local_shear[0][2] = Vweb_tmp.local_shear[0][2];
    Vweb[idx].local_shear[1][2] = Vweb_tmp.local_shear[1][2];
    Vweb[idx].local_shear[2][2] = Vweb_tmp.local_shear[2][2];
    Vweb[idx].filaid            = -1;

    /* count web elements */
    if(Vweb_tmp.lambda3 > lambda_threshold) {
      Nknots++;
      Vweb[idx].type = KNOT_TYPE;
    }
    if(Vweb_tmp.lambda2 > lambda_threshold && Vweb_tmp.lambda3 < lambda_threshold) {
      Nfilaments++;
      Vweb[idx].type = FILAMENT_TYPE;
    }
    if(Vweb_tmp.lambda1 > lambda_threshold && Vweb_tmp.lambda2 < lambda_threshold) {
      Nsheets++;
      Vweb[idx].type = SHEET_TYPE;
    }
    if(Vweb_tmp.lambda1 < lambda_threshold) {
      Nvoids++;
      Vweb[idx].type = VOID_TYPE;
    }
} // for(Nnodes)
  
#else // VWEB_ASCII
  
  // figure out swap status
  fread(&one, sizeof(int32_t), 1, fpin);
  if(one == 1)    swap = 0;
  else            swap = 1;

  ReadUInt64(fpin, &Nnodes,       swap);
  ReadUInt64(fpin, &L,            swap);
  ReadFloat (fpin, &BoxSize_tmp,  swap);

  // minimal consistency check
  if(l1dim != 0 && l1dim != L) {
    fprintf(stderr,"the files you are merging are not for the same grid level: L=%"PRIu64" vs. l1dim=%"PRIu64"\nABORTING\n",L,l1dim);
    exit(0);
  }
  if(BoxSize > 0. && BoxSize != BoxSize_tmp) {
    fprintf(stderr,"the files you are merging are not for the same boxsize: BoxSize=%f vs. BoxSize_tmp=%f\nABORTING\n",BoxSize,BoxSize_tmp);
    exit(0);
  }

  // we can safely use L and BoxSize_tmp
  l1dim   = (uint64_t) L;
  BoxSize = BoxSize_tmp;
  
  // allocate space for full(!) Vweb
  // TODO: here we assume that Vweb is given on a *regular* grid!!!
  if(Vweb == NULL) {
    Vweb = (Vweb_t *) calloc(l1dim*l1dim*l1dim, sizeof(Vweb_t));
  }
  VwebNodes += Nnodes;
  
#ifdef VERBOSE
  fprintf(stderr,"o reading %"PRIu64" cells from file (swap=%d,l1dim=%"PRIu64")\n",Nnodes,swap,l1dim);
#endif

  // read in Vweb properties
  for(n=0; n<Nnodes; n++) {
    
    // read node into temporary structure
    ReadFloat(fpin, &(Vweb_tmp.x),           swap);
    ReadFloat(fpin, &(Vweb_tmp.y),           swap);
    ReadFloat(fpin, &(Vweb_tmp.z),           swap);
    ReadFloat(fpin, &(Vweb_tmp.dens),        swap);
    ReadFloat(fpin, &(Vweb_tmp.Vx),          swap);
    ReadFloat(fpin, &(Vweb_tmp.Vy),          swap);
    ReadFloat(fpin, &(Vweb_tmp.Vz),          swap);
    ReadFloat(fpin, &(Vweb_tmp.Wx),          swap);
    ReadFloat(fpin, &(Vweb_tmp.Wy),          swap);
    ReadFloat(fpin, &(Vweb_tmp.Wz),          swap);
    ReadFloat(fpin, &(Vweb_tmp.lambda1),     swap);
    ReadFloat(fpin, &(Vweb_tmp.lambda2),     swap);
    ReadFloat(fpin, &(Vweb_tmp.lambda3),     swap);
    ReadFloat(fpin, &(Vweb_tmp.local_shear[0][0]),     swap);
    ReadFloat(fpin, &(Vweb_tmp.local_shear[1][0]),     swap);
    ReadFloat(fpin, &(Vweb_tmp.local_shear[2][0]),     swap);
    ReadFloat(fpin, &(Vweb_tmp.local_shear[0][1]),     swap);
    ReadFloat(fpin, &(Vweb_tmp.local_shear[1][1]),     swap);
    ReadFloat(fpin, &(Vweb_tmp.local_shear[2][1]),     swap);
    ReadFloat(fpin, &(Vweb_tmp.local_shear[0][2]),     swap);
    ReadFloat(fpin, &(Vweb_tmp.local_shear[1][2]),     swap);
    ReadFloat(fpin, &(Vweb_tmp.local_shear[2][2]),     swap);
#ifdef UonGrid
    ReadFloat(fpin, &(Vweb_tmp.u),         swap);
#endif
    
    // calculate position (i,j,k) in actual Vweb[] array from (x,y,z)
    get_ijk(Vweb_tmp.x,Vweb_tmp.y,Vweb_tmp.z, &i,&j,&k);
    
    // copy over to actual Vweb[] array
    idx = VwebIndex(i,j,k);
    Vweb[idx].x                 = Vweb_tmp.x;
    Vweb[idx].y                 = Vweb_tmp.y;
    Vweb[idx].z                 = Vweb_tmp.z;
    Vweb[idx].dens              = Vweb_tmp.dens;
    Vweb[idx].Vx                = Vweb_tmp.Vx;
    Vweb[idx].Vy                = Vweb_tmp.Vy;
    Vweb[idx].Vz                = Vweb_tmp.Vz;
    Vweb[idx].Wx                = Vweb_tmp.Wx;
    Vweb[idx].Wy                = Vweb_tmp.Wy;
    Vweb[idx].Wz                = Vweb_tmp.Wz;
    Vweb[idx].lambda1           = Vweb_tmp.lambda1;
    Vweb[idx].lambda2           = Vweb_tmp.lambda2;
    Vweb[idx].lambda3           = Vweb_tmp.lambda3;
    Vweb[idx].local_shear[0][0] = Vweb_tmp.local_shear[0][0];
    Vweb[idx].local_shear[1][0] = Vweb_tmp.local_shear[1][0];
    Vweb[idx].local_shear[2][0] = Vweb_tmp.local_shear[2][0];
    Vweb[idx].local_shear[0][1] = Vweb_tmp.local_shear[0][1];
    Vweb[idx].local_shear[1][1] = Vweb_tmp.local_shear[1][1];
    Vweb[idx].local_shear[2][1] = Vweb_tmp.local_shear[2][1];
    Vweb[idx].local_shear[0][2] = Vweb_tmp.local_shear[0][2];
    Vweb[idx].local_shear[1][2] = Vweb_tmp.local_shear[1][2];
    Vweb[idx].local_shear[2][2] = Vweb_tmp.local_shear[2][2];
#ifdef UonGrid
    Vweb[idx].u                 = Vweb_tmp.u;
#endif
    Vweb[idx].filaid            = -1;
    
    /* count web elements */
    if(Vweb_tmp.lambda3 > lambda_threshold) {
      Nknots++;
      Vweb[idx].type = KNOT_TYPE;
    }
    if(Vweb_tmp.lambda2 > lambda_threshold && Vweb_tmp.lambda3 < lambda_threshold) {
      Nfilaments++;
      Vweb[idx].type = FILAMENT_TYPE;
    }
    if(Vweb_tmp.lambda1 > lambda_threshold && Vweb_tmp.lambda2 < lambda_threshold) {
      Nsheets++;
      Vweb[idx].type = SHEET_TYPE;
    }
    if(Vweb_tmp.lambda1 < lambda_threshold) {
      Nvoids++;
      Vweb[idx].type = VOID_TYPE;
    }
  
  } // for(Nnodes)
#endif // VWEB_ASCII
}


////////////////////////////////////////////////////////////////////////////////////////


/*
 Read a possibly byte swapped unsigned integer
 */
int ReadUInt32(FILE *fptr,uint32_t *n,int swap)
{
  unsigned char *cptr,tmp;
  
  if(sizeof(int) != 4)
   {
    fprintf(stderr,"ReadInt: sizeof(int)=%ld and not 4\n",sizeof(int));
    exit(0);
   }
  
  if (fread(n,4,1,fptr) != 1)
    return(FALSE);
  if (swap) {
    cptr = (unsigned char *)n;
    tmp     = cptr[0];
    cptr[0] = cptr[3];
    cptr[3] = tmp;
    tmp     = cptr[1];
    cptr[1] = cptr[2];
    cptr[2] = tmp;
  }
  return(TRUE);
}

/*
 Read a possibly byte swapped unsigned long integer
 */
int ReadUInt64(FILE *fptr, uint64_t *n,int swap)
{
  unsigned char *cptr,tmp;
  
  if(sizeof(uint64_t) == 4)
   {
    if (fread(n,4,1,fptr) != 1)
      return(FALSE);
    if (swap) {
      cptr = (unsigned char *)n;
      tmp     = cptr[0];
      cptr[0] = cptr[3];
      cptr[3] = tmp;
      tmp     = cptr[1];
      cptr[1] = cptr[2];
      cptr[2] = tmp;
    }
   }
  else if(sizeof(uint64_t) == 8)
   {
    if (fread(n,8,1,fptr) != 1)
      return(FALSE);
    if (swap) {
      cptr = (unsigned char *)n;
      tmp     = cptr[0];
      cptr[0] = cptr[7];
      cptr[7] = tmp;
      tmp     = cptr[1];
      cptr[1] = cptr[6];
      cptr[6] = tmp;
      tmp     = cptr[2];
      cptr[2] = cptr[5];
      cptr[5] = tmp;
      tmp     = cptr[3];
      cptr[3] = cptr[4];
      cptr[4] = tmp;
    }
   }
  else
   {
    fprintf(stderr,"ReadUInt64: something wrong...cannot read long\n");
    exit(0);
   }
  
  return(TRUE);
}

/*
 Read a possibly byte swapped floating point number
 Assume IEEE format
 */
int ReadFloat2(FILE *fptr,float *n, int swap)
{
  unsigned char *cptr,tmp;
  
  if(sizeof(float) != 4)
   {
    fprintf(stderr,"ReadFloat: sizeof(float)=%ld and not 4\n",sizeof(float));
    exit(0);
   }
  
  if (fread(n,4,1,fptr) != 1)
    return(FALSE);
  if (swap)
   {
    cptr = (unsigned char *)n;
    tmp     = cptr[0];
    cptr[0] = cptr[3];
    cptr[3] = tmp;
    tmp     = cptr[1];
    cptr[1] = cptr[2];
    cptr[2] = tmp;
   }
  return(TRUE);
}


/*=================================================================================================
 * get_ijk()
 *                  NOTE: makes use of global variable l1dim!
 *=================================================================================================*/
void get_ijk(float x, float y, float z, uint64_t *i, uint64_t *j, uint64_t *k)
{
  *i = (uint64_t) floor(x*((float)l1dim-0.5)/BoxSize);
  *j = (uint64_t) floor(y*((float)l1dim-0.5)/BoxSize);
  *k = (uint64_t) floor(z*((float)l1dim-0.5)/BoxSize);
  
}

#ifdef RECURSION
/*=================================================================================================
 * friends_of_friends()
 *=================================================================================================*/
void friends_of_friends(uint64_t i, uint64_t j, uint64_t k)
{
  int64_t in,jn,kn, ii,jj,kk; // use int64_t (and not uint64_t) to allow for backward searches
  uint64_t idx;
 
  // loop over all (forward only!) nodes
  for(in=i-1; in<=i+1; in++) {
    for(jn=j-1; jn<=j+1; jn++) {
      for(kn=k-1; kn<=k+1; kn++) {
        ii  = (in+l1dim) % (l1dim);
        jj  = (jn+l1dim) % (l1dim);
        kk  = (kn+l1dim) % (l1dim);
        idx = VwebIndex(ii,jj,kk);
        
        // is neighbour node of type filament
        if(Vweb[idx].type_new == FILAMENT_TYPE) {
          
          // check whether this nodes has already been considered
          if(Vweb[idx].filaid == -1) {
            
            // mark Vweb nodes as belonging to this filament
            Vweb[idx].filaid = nFilaments;

            // add current node to filament
            Filament[nFilaments-1].Nnodes++;
            Filament[nFilaments-1].idx = (uint64_t *) realloc(Filament[nFilaments-1].idx, Filament[nFilaments-1].Nnodes*sizeof(uint64_t));
            Filament[nFilaments-1].idx[Filament[nFilaments-1].Nnodes-1] = idx;

            // recursively check neighbours of this node
            friends_of_friends(ii,jj,kk);
          }
        }
      } // kn
    } // jn
  } // in
}
#endif
