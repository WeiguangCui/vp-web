pro plot_Vweb

;================================================
;           USER PARAMETERS
;================================================
DATApath  = './'
SIMUname  = 'Vweb_lcdm_057.z0.000.AHF_halos'
Lgrid     = 64l    ; grid size used to generate Vweb
BoxSize   = 250.    ; simulation boxsize
lambda_th = 0.14    ; cosmic web lambda threshold

zlo       = 0.
zup       = zlo+BoxSize/float(Lgrid) ; only plot a slice of thickness one grid cell

Lstring   = strtrim(string(fix(Lgrid),FORMAT='(i06)'),2)
ltstring  = strtrim(string(lambda_th,FORMAT='(f4.2)'),2)
file_in   = DATApath+SIMUname+'.'+Lstring+'.Vweb-ascii'
epsname   = 'Vweb_'+SIMUname+'.'+Lstring+'.lambda'+ltstring+'.eps'
chsz      = 1       ; charsize
chth      = 1       ; charthick

;================================================
;           OUTPUT FILE
;================================================
print,' + opening ',epsname
time0=systime(1)
!p.font = -1
set_plot,'ps'
device,/color,filename=epsname,xsize=40,ysize=20,/encapsulated
time1=systime(1)
print,'   ... which took ',(time1-time0)/60.,' min.'


;================================================
;         READ Vweb2ascii STRUCTURE
;================================================
FULL_VWEB_SET = 0
if FULL_VWEB_SET EQ 1 then begin
  struc={x:0.0,$
         y:0.0,$
         z:0.0,$
         d:0.0,$
         vx:0.0,$
         vy:0.0,$
         vz:0.0,$
         Wx:0.0,$
         Wy:0.0,$
         Wz:0.0,$
         l1:0.0,$
         l2:0.0,$
         l3:0.0,$
         e1x:0.0,$
         e1y:0.0,$
         e1z:0.0,$
         e2x:0.0,$
         e2y:0.0,$
         e2z:0.0,$
         e3x:0.0,$
         e3y:0.0,$
         e3z:0.0}
endif else begin
  struc={x:0.0,$
         y:0.0,$
         z:0.0,$
         d:0.0,$
         l1:0.0,$
         l2:0.0,$
         l3:0.0,$
         webid:0.0}
endelse

print,' + reading Vweb'
time0=systime(1)
aweb=replicate(struc,Lgrid*Lgrid*Lgrid)
web_header=strarr(1)
openr,unit,file_in,/get_lun
readf,unit,web_header
readf,unit,aweb
free_lun,unit
time1=systime(1)
print,'   ... which took ',(time1-time0)/60.,' min.'

;================================================
;         COSMIC WEB DECOMPOSITION
;================================================
print,' + performing Cosmic Web Decomposition'
time0=systime(1)
aknot    = where(aweb.z ge zlo and aweb.z le zup and (aweb.l3 gt lambda_th),                          nknot)
afila    = where(aweb.z ge zlo and aweb.z le zup and (aweb.l2 gt lambda_th and aweb.l3 lt lambda_th), nfila)
ashee    = where(aweb.z ge zlo and aweb.z le zup and (aweb.l1 gt lambda_th and aweb.l2 lt lambda_th), nshee)
avoid    = where(aweb.z ge zlo and aweb.z le zup and (aweb.l1 lt lambda_th),                          nvoid)
aplotden = where(aweb.z ge zlo and aweb.z le zup, ndens)
time1=systime(1)
print,'   ... which took ',(time1-time0)/60.,' min.'
print,'  -> web statistic:'
print,'       nknot = ',nknot,float(nknot)/float(ndens)*100.
print,'       nfila = ',nfila,float(nfila)/float(ndens)*100.
print,'       nshee = ',nshee,float(nshee)/float(ndens)*100.
print,'       nvoid = ',nvoid,float(nvoid)/float(ndens)*100.
print,'       ndens = ',ndens


;================================================
;         PLOT COSMIC WEB DECOMPOSITION
;================================================
print,' + plotting Vweb'
time0=systime(1)
!p.multi=[0,2,1]
simsize=0.8
xa=simsize*[-1,1,1,-1,-1]
ya=simsize*[-1,-1,1,1,-1]

usersym,xa,ya,/fill
clevs=6*findgen(30)/29.-3

loadct,0
plot,aweb[avoid].x,aweb[avoid].y,thick=chth,xthick=chth,ythick=th,xra=[0,BoxSize],yra=[0,BoxSize],xstyle=1,ystyle=1,charsize=chsz,charthick=chth,psym=3,/nodata,xtit='X [Mpc/h]',ytit='Y [Mpc/h]',tit='Vweb'
if n_elements(ashee) GT 1 then oplot,aweb[ashee].x,aweb[ashee].y,psym=8,col=98
if n_elements(afila) GT 1 then oplot,aweb[afila].x,aweb[afila].y,psym=8,col=198
if n_elements(aknot) GT 1 then oplot,aweb[aknot].x,aweb[aknot].y,psym=8
loadct,12
contour,alog10(aweb[aplotden].d),aweb[aplotden].x,aweb[aplotden].y,levels=[0],/irregular,/overplot,col=198


;loadct,0
;contour,alog10(aweb[aplotden].d),aweb[aplotden].x,aweb[aplotden].y,levels=clevs,/irregular,/fill,xra=[0,BoxSize],yra=[0,BoxSize],xstyle=1,ystyle=1,charsize=chsz,charthick=chth,xtit='X [Mpc/h]',ytit='Y [Mpc/h]',tit='Density',/nodata
;loadct,39
;contour,alog10(aweb[aplotden].d),aweb[aplotden].x,aweb[aplotden].y,levels=clevs,/irregular,/fill,/overplot
;loadct,0
;contour,alog10(aweb[aplotden].d),aweb[aplotden].x,aweb[aplotden].y,levels=[0],/irregular,/overplot,col=0

device,/close
time1=systime(1)
print,'   ... which took ',(time1-time0)/60.,' min.'

end



