#include "../param.h"
#include "../tdef.h"

#ifndef SMOOTH_GRID_INCLUDED
#define SMOOTH_GRID_INCLUDED

void     Gaussian_smooth_grid  (gridls *cur_grid);
void     smooth_grid      (gridls *cur_grid);

#endif

