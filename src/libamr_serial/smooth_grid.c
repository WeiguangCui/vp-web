#include <stddef.h>
#include <math.h>
#ifdef WITH_OPENMP
#include <omp.h>
#endif

/* the important definitions have to be included first */
#include "../common.h"
#include "../param.h"
#include "../tdef.h"

/* ...and now for the actual includes */
#include "amr_serial.h"
#include "../libutility/utility.h"
#include "../libio/utilMath.h"  //For M_PI

#ifdef GAUSSIAN_SMOOTHING
void createFilter(double gKernel[5][5][5])
{
  // set standard deviation to 1.0
  double sigma = 1.0;
  double r, s = 2.0 * sigma * sigma;
  
  // sum is for normalization
  double sum = 0.0;
  
  // generate 5x5 kernel
  for (int x = -2; x <= 2; x++) {
    for(int y = -2; y <= 2; y++) {
      for(int z = -2; y <= 2; y++) {
        r = sqrt(x*x + y*y + z*z);
        gKernel[x + 2][y + 2][z + 2] = (exp(-(r*r)/s))/(M_PI * s);
        sum += gKernel[x + 2][y + 2][z + 2];
      }
    }
  }
  
  // normalize the Kernel
  for(int i = 0; i < 5; ++i)
    for(int j = 0; j < 5; ++j)
      for(int k = 0; j < 5; ++j)
        gKernel[i][j][k] /= sum;
}


/*=============================================================================
 * Gaussian smoothing
 *=============================================================================*/
void smooth_grid(gridls *cur_grid)
{
#ifdef WITH_OPENMP
  long          ipquad;
#endif
  
  pqptr         cur_pquad;
  cqptr         cur_cquad, icur_cquad;
  nqptr         cur_nquad, icur_nquad;
  nptr          cur_node;
  long          cur_x, cur_y, cur_z;
  nptr          mhd_nodes[5][5][5];
  int           i, j, k, x, y, z;
  double        Gaussian[5][5][5], Gnorm, r, s, cellsize, Rsmooth, sigma;
  
  
  /*---------------------------------------
   * 0. fill the Gaussian smoothing matrix
   *---------------------------------------*/
  cellsize = simu.boxsize/cur_grid->l1dim;   // [Mpc/h]
  Rsmooth  = 5.0;                            // [Mpc/h]
  printf("Gaussian Smoothing kernel scale: %f Mpc/h \n", Rsmooth);
  
  // generate 5x5x5 kernel
  sigma = 1.0;
  s     = 2.0 * pow2(sigma*Rsmooth);
  for (x = -2; x <= 2; x++) {
    for(y = -2; y <= 2; y++) {
      for(z = -2; y <= 2; y++) {
        r = sqrt(x*x + y*y + z*z) * cellsize;
        Gaussian[x+2][y+2][z+2] = exp(-pow2(r)/s);
        
        // keep track of the normalisation
        Gnorm += Gaussian[x+2][y+2][z+2];
      }
    }
  }
  
  
  /*==============================================================
   * three loops over cur_grid:
   *  1. reset temporary values
   *  2. smooth storing results in temporary storage
   *  3. copy temporary storage over to actual variables
   *==============================================================*/
  
  /*----------------
   * 1. init-loop
   *----------------*/
#ifdef WITH_OPENMP
#pragma omp parallel private(ipquad, cur_pquad,  cur_cquad, icur_cquad, cur_nquad, icur_nquad, cur_node, cur_x, cur_y, cur_z)  shared(cur_grid)
#pragma omp for schedule(static)
  for(ipquad=0; ipquad<cur_grid->no_pquad; ipquad++) {
    cur_pquad=cur_grid->pquad_array[ipquad];
#else
    for(cur_pquad=cur_grid->pquad; cur_pquad != NULL; cur_pquad=cur_pquad->next) {
#endif
      cur_z = cur_pquad->z;
      
      for(cur_cquad = cur_pquad->loc; cur_cquad < cur_pquad->loc + cur_pquad->length; cur_cquad++, cur_z++) {
        for(icur_cquad  = cur_cquad; icur_cquad != NULL; icur_cquad  = icur_cquad->next) {
          cur_y = icur_cquad->y;
          
          for(cur_nquad = icur_cquad->loc; cur_nquad < icur_cquad->loc + icur_cquad->length; cur_nquad++, cur_y++) {
            for(icur_nquad  = cur_nquad; icur_nquad != NULL; icur_nquad  = icur_nquad->next) {
              cur_x = icur_nquad->x;
              
              for(cur_node = icur_nquad->loc; cur_node < icur_nquad->loc + icur_nquad->length; cur_node++, cur_x++) {
                cur_node->densVtmp[X]   = 0.0;
                cur_node->densVtmp[Y]   = 0.0;
                cur_node->densVtmp[Z]   = 0.0;
                cur_node->force.temp[0] = 0.0;
#ifdef UonGrid
                cur_node->force.temp[1] = 0.0;
#endif
#ifdef PWEB
                cur_node->force.temp[2] = 0.0;
#endif
              }
            }
          }
        }
      }
    }
    
    /*----------------
     * 2. smooth-loop
     *----------------*/
#ifdef WITH_OPENMP
#pragma omp parallel private(ipquad, cur_pquad,  cur_cquad, icur_cquad, cur_nquad, icur_nquad, cur_node, cur_x, cur_y, cur_z)  shared(cur_grid)
#pragma omp for schedule(static)
    for(ipquad=0; ipquad<cur_grid->no_pquad; ipquad++) {
      cur_pquad=cur_grid->pquad_array[ipquad];
#else
      for(cur_pquad=cur_grid->pquad; cur_pquad != NULL; cur_pquad=cur_pquad->next) {
#endif
        cur_z = cur_pquad->z;
        
        for(cur_cquad = cur_pquad->loc; cur_cquad < cur_pquad->loc + cur_pquad->length; cur_cquad++, cur_z++) {
          for(icur_cquad  = cur_cquad; icur_cquad != NULL; icur_cquad  = icur_cquad->next) {
            cur_y = icur_cquad->y;
            
            for(cur_nquad = icur_cquad->loc; cur_nquad < icur_cquad->loc + icur_cquad->length; cur_nquad++, cur_y++) {
              for(icur_nquad  = cur_nquad; icur_nquad != NULL; icur_nquad  = icur_nquad->next) {
                cur_x = icur_nquad->x;
                
                for(cur_node = icur_nquad->loc; cur_node < icur_nquad->loc + icur_nquad->length; cur_node++, cur_x++) {
                  /*  smooth over 5x5x5 neighbours */
                  get_MHDnodes(cur_grid, cur_pquad, cur_z, cur_y, cur_x, mhd_nodes);
                  for(k = 0; k < 5; k++){
                    for(j = 0; j < 5; j++){
                      for(i = 0; i < 5; i++){
                        if(mhd_nodes[k][j][i] != NULL) {
                          cur_node->densVtmp[X]   += Gaussian[k][j][i] * mhd_nodes[k][j][i]->densV[X];
                          cur_node->densVtmp[Y]   += Gaussian[k][j][i] * mhd_nodes[k][j][i]->densV[Y];
                          cur_node->densVtmp[Z]   += Gaussian[k][j][i] * mhd_nodes[k][j][i]->densV[Z];
                          cur_node->force.temp[0] += Gaussian[k][j][i] * mhd_nodes[k][j][i]->dens;
#ifdef UonGrid
                          cur_node->force.temp[1] += Gaussian[k][j][i] * mhd_nodes[k][j][i]->u;
#endif
#ifdef PWEB
                          cur_node->force.temp[2] += Gaussian[k][j][i] * mhd_nodes[k][j][i]->pot;
#endif
                        }
                      }
                    }
                  }
                  
                  
                  /* store smoothed momentum density in densVtmp[] and mass density in force.forces[X] */
                  cur_node->densVtmp[X]   /= Gnorm;
                  cur_node->densVtmp[Y]   /= Gnorm;
                  cur_node->densVtmp[Z]   /= Gnorm;
                  cur_node->force.temp[0] /= Gnorm;
#ifdef UonGrid
                  cur_node->force.temp[1] /= Gnorm;
#endif
#ifdef PWEB
                  cur_node->force.temp[2] /= Gnorm;
#endif

                }
              }
            }
          }
        }
      }
      
      /*----------------
       * 3. copy-loop
       *----------------*/
#ifdef WITH_OPENMP
#pragma omp parallel private(ipquad, cur_pquad,  cur_cquad, icur_cquad, cur_nquad, icur_nquad, cur_node, cur_x, cur_y, cur_z)  shared(cur_grid)
#pragma omp for schedule(static)
      for(ipquad=0; ipquad<cur_grid->no_pquad; ipquad++) {
        cur_pquad=cur_grid->pquad_array[ipquad];
#else
        for(cur_pquad=cur_grid->pquad; cur_pquad != NULL; cur_pquad=cur_pquad->next) {
#endif
          cur_z = cur_pquad->z;
          
          for(cur_cquad = cur_pquad->loc; cur_cquad < cur_pquad->loc + cur_pquad->length; cur_cquad++, cur_z++) {
            for(icur_cquad  = cur_cquad; icur_cquad != NULL; icur_cquad  = icur_cquad->next) {
              cur_y = icur_cquad->y;
              
              for(cur_nquad = icur_cquad->loc; cur_nquad < icur_cquad->loc + icur_cquad->length; cur_nquad++, cur_y++) {
                for(icur_nquad  = cur_nquad; icur_nquad != NULL; icur_nquad  = icur_nquad->next) {
                  cur_x = icur_nquad->x;
                  
                  for(cur_node = icur_nquad->loc; cur_node < icur_nquad->loc + icur_nquad->length; cur_node++, cur_x++) {
                    cur_node->densV[X]  = cur_node->densVtmp[X];
                    cur_node->densV[Y]  = cur_node->densVtmp[Y];
                    cur_node->densV[Z]  = cur_node->densVtmp[Z];
                    cur_node->dens      = cur_node->force.temp[0];
#ifdef UonGrid
                    cur_node->u         = cur_node->force.temp[1];
#endif
#ifdef PWEB
                    cur_node->pot       = cur_node->force.temp[2];
#endif
                  }
                }
                
              }
            }
          }
        }
        
        
      }
        
#else  //Instead of Gaussian smoothing, we do TSC/MHD smoothing
      
/*=============================================================================
 * unsophisticated average smoothing
 *=============================================================================*/
void smooth_grid(gridls *cur_grid)
{
#ifdef WITH_OPENMP
  long          ipquad;
#endif

  pqptr         cur_pquad;
  cqptr         cur_cquad, icur_cquad;
  nqptr         cur_nquad, icur_nquad;
  nptr          cur_node;
  long          cur_x, cur_y, cur_z;
  nptr          tsc_nodes[3][3][3];
  nptr          mhd_nodes[5][5][5];
  int           i, j, k, nsmooth;

  /*==============================================================
   * three loops over cur_grid:
   *  1. reset temporary values
   *  2. smooth storing results in temporary storage
   *  3. copy temporary storage over to actual variables
   *==============================================================*/

  /*----------------
   * 1. init-loop
   *----------------*/
#ifdef WITH_OPENMP
#pragma omp parallel private(ipquad, cur_pquad,  cur_cquad, icur_cquad, cur_nquad, icur_nquad, cur_node, cur_x, cur_y, cur_z)  shared(cur_grid)
#pragma omp for schedule(static)
  for(ipquad=0; ipquad<cur_grid->no_pquad; ipquad++) {
    cur_pquad=cur_grid->pquad_array[ipquad];
#else
  for(cur_pquad=cur_grid->pquad; cur_pquad != NULL; cur_pquad=cur_pquad->next) {
#endif
      cur_z = cur_pquad->z;

      for(cur_cquad = cur_pquad->loc;
          cur_cquad < cur_pquad->loc + cur_pquad->length;
          cur_cquad++, cur_z++)
       {
        for(icur_cquad  = cur_cquad;
            icur_cquad != NULL;
            icur_cquad  = icur_cquad->next)
         {
          cur_y = icur_cquad->y;

          for(cur_nquad = icur_cquad->loc;
              cur_nquad < icur_cquad->loc + icur_cquad->length;
              cur_nquad++, cur_y++)
           {
            for(icur_nquad  = cur_nquad;
                icur_nquad != NULL;
                icur_nquad  = icur_nquad->next)
             {
              cur_x = icur_nquad->x;

              for(cur_node = icur_nquad->loc;
                  cur_node < icur_nquad->loc + icur_nquad->length;
                  cur_node++, cur_x++)
               {
                cur_node->densVtmp[X]   = 0.0;
                cur_node->densVtmp[Y]   = 0.0;
                cur_node->densVtmp[Z]   = 0.0;
                cur_node->force.temp[0] = 0.0;
#ifdef UonGrid
                cur_node->force.temp[1] = 0.0;
#endif
#ifdef PWEB
                cur_node->force.temp[2] = 0.0;
#endif
               }
             }
           }
         }
       }
    }

    /*----------------
     * 2. smooth-loop
     *----------------*/
#ifdef WITH_OPENMP
#pragma omp parallel private(ipquad, cur_pquad,  cur_cquad, icur_cquad, cur_nquad, icur_nquad, cur_node, cur_x, cur_y, cur_z)  shared(cur_grid)
#pragma omp for schedule(static)
  for(ipquad=0; ipquad<cur_grid->no_pquad; ipquad++) {
    cur_pquad=cur_grid->pquad_array[ipquad];
#else
  for(cur_pquad=cur_grid->pquad; cur_pquad != NULL; cur_pquad=cur_pquad->next) {
#endif
      cur_z = cur_pquad->z;

      for(cur_cquad = cur_pquad->loc;
          cur_cquad < cur_pquad->loc + cur_pquad->length;
          cur_cquad++, cur_z++)
       {
         for(icur_cquad  = cur_cquad;
             icur_cquad != NULL;
             icur_cquad  = icur_cquad->next)
          {
           cur_y = icur_cquad->y;

           for(cur_nquad = icur_cquad->loc;
               cur_nquad < icur_cquad->loc + icur_cquad->length;
               cur_nquad++, cur_y++)
            {
              for(icur_nquad  = cur_nquad;
                  icur_nquad != NULL;
                  icur_nquad  = icur_nquad->next)
               {
                cur_x = icur_nquad->x;

                for(cur_node = icur_nquad->loc;
                    cur_node < icur_nquad->loc + icur_nquad->length;
                    cur_node++, cur_x++)
                 {
                  /* keep track of how many cells have been used during the smoothing */
                  nsmooth = 0;

                  /*  smooth over neighbours */
#ifdef MHD_SMOOTHING
                  get_MHDnodes(cur_grid, cur_pquad, cur_z, cur_y, cur_x, mhd_nodes);
                  for(k = 0; k < 5; k++){
                    for(j = 0; j < 5; j++){
                      for(i = 0; i < 5; i++){
                        if(mhd_nodes[k][j][i] != NULL) {
                          nsmooth++;
                          cur_node->densVtmp[X]   += mhd_nodes[k][j][i]->densV[X];
                          cur_node->densVtmp[Y]   += mhd_nodes[k][j][i]->densV[Y];
                          cur_node->densVtmp[Z]   += mhd_nodes[k][j][i]->densV[Z];
                          cur_node->force.temp[0] += mhd_nodes[k][j][i]->dens;
#ifdef UonGrid
                          cur_node->force.temp[1] += mhd_nodes[k][j][i]->u;
#endif
#ifdef PWEB
                          cur_node->force.temp[2] += mhd_nodes[k][j][i]->pot;
#endif
                        }
                      }
                    }
                  }
#else
                  tsc_nodes[1][1][1] = cur_node;
                  get_TSCnodes(cur_grid, cur_pquad, icur_cquad, icur_nquad, tsc_nodes, &cur_z, &cur_y, &cur_x);
                  for(k = 0; k < 3; k++){
                    for(j = 0; j < 3; j++){
                      for(i = 0; i < 3; i++){
                        if(tsc_nodes[k][j][i] != NULL) {
                          nsmooth++;
                          cur_node->densVtmp[X]   += tsc_nodes[k][j][i]->densV[X];
                          cur_node->densVtmp[Y]   += tsc_nodes[k][j][i]->densV[Y];
                          cur_node->densVtmp[Z]   += tsc_nodes[k][j][i]->densV[Z];
                          cur_node->force.temp[0] += tsc_nodes[k][j][i]->dens;
#ifdef UonGrid
                          cur_node->force.temp[1] += tsc_nodes[k][j][i]->u;
#endif
#ifdef PWEB
                          cur_node->force.temp[2] += tsc_nodes[k][j][i]->pot;
#endif
                        }
                      }
                    }
                  }
#endif

                  /* store smoothed momentum density in densVtmp[] and mass density in force.forces[X] */
                  cur_node->densVtmp[X]   /= (double)(nsmooth);
                  cur_node->densVtmp[Y]   /= (double)(nsmooth);
                  cur_node->densVtmp[Z]   /= (double)(nsmooth);
                  cur_node->force.temp[0] /= (double)(nsmooth);
#ifdef UonGrid
                  cur_node->force.temp[1] /= (double)(nsmooth);
#endif
#ifdef PWEB
                  cur_node->force.temp[2] /= (double)(nsmooth);
#endif
                 }
               }
            }
          }
       }
     }

    /*----------------
     * 3. copy-loop
     *----------------*/
#ifdef WITH_OPENMP
#pragma omp parallel private(ipquad, cur_pquad,  cur_cquad, icur_cquad, cur_nquad, icur_nquad, cur_node, cur_x, cur_y, cur_z)  shared(cur_grid)
#pragma omp for schedule(static)
    for(ipquad=0; ipquad<cur_grid->no_pquad; ipquad++) {
      cur_pquad=cur_grid->pquad_array[ipquad];
#else
    for(cur_pquad=cur_grid->pquad; cur_pquad != NULL; cur_pquad=cur_pquad->next) {
#endif
        cur_z = cur_pquad->z;

        for(cur_cquad = cur_pquad->loc;
            cur_cquad < cur_pquad->loc + cur_pquad->length;
            cur_cquad++, cur_z++)
         {
          for(icur_cquad  = cur_cquad;
              icur_cquad != NULL;
              icur_cquad  = icur_cquad->next)
           {
            cur_y = icur_cquad->y;

            for(cur_nquad = icur_cquad->loc;
                cur_nquad < icur_cquad->loc + icur_cquad->length;
                cur_nquad++, cur_y++)
             {
              for(icur_nquad  = cur_nquad;
                  icur_nquad != NULL;
                  icur_nquad  = icur_nquad->next)
               {
                cur_x = icur_nquad->x;

                for(cur_node = icur_nquad->loc;
                    cur_node < icur_nquad->loc + icur_nquad->length;
                    cur_node++, cur_x++)
                 {
                  cur_node->densV[X]  = cur_node->densVtmp[X];
                  cur_node->densV[Y]  = cur_node->densVtmp[Y];
                  cur_node->densV[Z]  = cur_node->densVtmp[Z];
                  cur_node->dens      = cur_node->force.temp[0];
#ifdef UonGrid
                  cur_node->u         = cur_node->force.temp[1];
#endif
#ifdef PWEB
                  cur_node->pot       = cur_node->force.temp[2];
#endif
                 }
               }

             }
           }
         }
      }


}
#endif
